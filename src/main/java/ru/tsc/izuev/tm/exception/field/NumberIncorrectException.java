package ru.tsc.izuev.tm.exception.field;

public class NumberIncorrectException extends AbstractFieldException {

    public NumberIncorrectException() {
        super("Error! Number is incorrect...");
    }

    public NumberIncorrectException(final String value, Throwable cause) {
        super("Error! Value \"" + value + "\" is incorrect...");
    }
}
