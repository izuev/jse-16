package ru.tsc.izuev.tm.api.controller;

public interface ITaskController {

    void createTask();

    void updateTaskById();

    void updateTaskByIndex();

    void startTaskById();

    void startTaskByIndex();

    void changeTaskStatusById();

    void changeTaskStatusByIndex();

    void completeTaskById();

    void completeTaskByIndex();

    void showTasks();

    void showTasksByProjectId();

    void showTaskById();

    void showTaskByIndex();

    void clearTasks();

    void removeTaskById();

    void removeTaskByIndex();

}
