package ru.tsc.izuev.tm.api.controller;

public interface IProjectController {

    void createProject();

    void updateProjectById();

    void updateProjectByIndex();

    void startProjectById();

    void startProjectByIndex();

    void changeProjectStatusById();

    void changeProjectStatusByIndex();

    void completeProjectById();

    void completeProjectByIndex();

    void showProjects();

    void showProjectById();

    void showProjectByIndex();

    void clearProjects();

    void removeProjectById();

    void removeProjectByIndex();

}
