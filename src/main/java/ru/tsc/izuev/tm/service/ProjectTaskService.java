package ru.tsc.izuev.tm.service;

import ru.tsc.izuev.tm.api.repository.IProjectRepository;
import ru.tsc.izuev.tm.api.repository.ITaskRepository;
import ru.tsc.izuev.tm.api.service.IProjectTaskService;
import ru.tsc.izuev.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.izuev.tm.exception.entity.TaskNotFoundException;
import ru.tsc.izuev.tm.exception.field.ProjectIdEmptyException;
import ru.tsc.izuev.tm.exception.field.TaskIdEmptyException;
import ru.tsc.izuev.tm.model.Task;

import java.util.List;

public final class ProjectTaskService implements IProjectTaskService {

    public final IProjectRepository projectRepository;

    public final ITaskRepository taskRepository;

    public ProjectTaskService(final IProjectRepository projectRepository, final ITaskRepository taskRepository) {
        this.projectRepository = projectRepository;
        this.taskRepository = taskRepository;
    }

    @Override
    public Task bindTaskToProject(final String projectId, final String taskId) {
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        if (taskId == null || taskId.isEmpty()) throw new TaskIdEmptyException();
        if (projectRepository.notExistsById(projectId)) throw new ProjectNotFoundException();
        final Task task = taskRepository.findById(taskId);
        if (task == null) throw new TaskNotFoundException();
        task.setProjectId(projectId);
        return task;
    }

    @Override
    public void remoteProjectById(final String projectId) {
        if (projectId == null || projectId.isEmpty()) return;
        if (projectRepository.notExistsById(projectId)) return;
        final List<Task> tasks = taskRepository.findAllByProjectId(projectId);
        for (final Task task : tasks) taskRepository.removeById(task.getId());
        projectRepository.removeById(projectId);
    }

    @Override
    public Task unbindTaskFromProject(final String projectId, final String taskId) {
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        if (taskId == null || taskId.isEmpty()) throw new TaskIdEmptyException();
        if (projectRepository.notExistsById(projectId)) throw new ProjectNotFoundException();
        final Task task = taskRepository.findById(taskId);
        if (task == null) throw new TaskNotFoundException();
        task.setProjectId(null);
        return task;
    }

}
