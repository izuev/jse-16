package ru.tsc.izuev.tm.controller;

import ru.tsc.izuev.tm.api.controller.ITaskController;
import ru.tsc.izuev.tm.api.service.ITaskService;
import ru.tsc.izuev.tm.enumerated.Sort;
import ru.tsc.izuev.tm.enumerated.Status;
import ru.tsc.izuev.tm.model.Task;
import ru.tsc.izuev.tm.util.TerminalUtil;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.List;

public final class TaskController implements ITaskController {

    private static final DateFormat DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");

    private final ITaskService taskService;

    public TaskController(final ITaskService taskService) {
        this.taskService = taskService;
    }

    private void showTask(final Task task) {
        if (task == null) return;
        System.out.println("ID: " + task.getId());
        System.out.println("NAME: " + task.getName());
        System.out.println("DESCRIPTION: " + task.getDescription());
        System.out.println("STATUS: " + Status.toName(task.getStatus()));
        System.out.println("CREATED: " + DATE_FORMAT.format(task.getCreated()));
    }

    private void renderTasks(final List<Task> tasks) {
        if (tasks == null) return;
        int index = 1;
        for (final Task task : tasks) {
            if (task == null) continue;
            System.out.println(index + ". " + task.getId() + ": " + task.getName());
            index++;
        }
    }

    @Override
    public void createTask() {
        System.out.println("[CREATE TASK]");
        System.out.println("[ENTER NAME]");
        final String name = TerminalUtil.nextLine();
        System.out.println("[ENTER DESCRIPTION]");
        final String description = TerminalUtil.nextLine();
        taskService.create(name, description);
    }

    @Override
    public void updateTaskById() {
        System.out.println("[UPDATE TASK BY ID]");
        System.out.println("[ENTER ID]");
        final String id = TerminalUtil.nextLine();
        System.out.println("[ENTER NAME]");
        final String name = TerminalUtil.nextLine();
        System.out.println("[ENTER DESCRIPTION]");
        final String description = TerminalUtil.nextLine();
        taskService.updateById(id, name, description);
    }

    @Override
    public void updateTaskByIndex() {
        System.out.println("[UPDATE TASK BY INDEX]");
        System.out.println("[ENTER INDEX]");
        final Integer index = TerminalUtil.nextNumber();
        System.out.println("[ENTER NAME]");
        final String name = TerminalUtil.nextLine();
        System.out.println("[ENTER DESCRIPTION]");
        final String description = TerminalUtil.nextLine();
        taskService.updateByIndex(index - 1, name, description);
    }

    @Override
    public void startTaskById() {
        System.out.println("[START TASK BY ID]");
        System.out.println("[ENTER ID]");
        final String id = TerminalUtil.nextLine();
        taskService.changeStatusById(id, Status.IN_PROGRESS);
    }

    @Override
    public void startTaskByIndex() {
        System.out.println("[START TASK BY INDEX]");
        System.out.println("[ENTER INDEX]");
        final Integer index = TerminalUtil.nextNumber();
        taskService.changeStatusByIndex(index - 1, Status.IN_PROGRESS);
    }

    @Override
    public void changeTaskStatusById() {
        System.out.println("[CHANGE TASK BY ID]");
        System.out.println("[ENTER ID]");
        final String id = TerminalUtil.nextLine();
        System.out.println("[ENTER STATUS]");
        System.out.println(Arrays.toString(Status.values()));
        final String statusDisplayName = TerminalUtil.nextLine();
        final Status status = Status.toStatus(statusDisplayName);
        taskService.changeStatusById(id, status);
    }

    @Override
    public void changeTaskStatusByIndex() {
        System.out.println("[CHANGE TASK BY INDEX]");
        System.out.println("[ENTER INDEX]");
        final Integer index = TerminalUtil.nextNumber();
        System.out.println("[ENTER STATUS]");
        System.out.println(Arrays.toString(Status.values()));
        final String statusDisplayName = TerminalUtil.nextLine();
        final Status status = Status.toStatus(statusDisplayName);
        taskService.changeStatusByIndex(index - 1, status);
    }

    @Override
    public void completeTaskById() {
        System.out.println("[COMPLETE TASK BY ID]");
        System.out.println("[ENTER ID]");
        final String id = TerminalUtil.nextLine();
        taskService.changeStatusById(id, Status.COMPLETED);
    }

    @Override
    public void completeTaskByIndex() {
        System.out.println("[COMPLETE TASK BY INDEX]");
        System.out.println("[ENTER INDEX]");
        final Integer index = TerminalUtil.nextNumber();
        taskService.changeStatusByIndex(index - 1, Status.COMPLETED);
    }

    @Override
    public void showTasks() {
        System.out.println("[SHOW TASKS]");
        System.out.println("[ENTER SORT]");
        System.out.println(Arrays.toString(Sort.values()));
        final String sortType = TerminalUtil.nextLine();
        final Sort sort = Sort.toSort(sortType);
        final List<Task> tasks = taskService.findAll(sort);
        renderTasks(tasks);
    }

    @Override
    public void showTasksByProjectId() {
        System.out.println("[SHOW TASK BY PROJECT ID]");
        System.out.println("[ENTER PROJECT ID]");
        final String id = TerminalUtil.nextLine();
        final List<Task> tasks = taskService.findAllByProjectId(id);
        renderTasks(tasks);
    }

    @Override
    public void showTaskById() {
        System.out.println("[SHOW TASK BY ID]");
        System.out.println("[ENTER ID]");
        final String id = TerminalUtil.nextLine();
        final Task task = taskService.findById(id);
        showTask(task);
    }

    @Override
    public void showTaskByIndex() {
        System.out.println("[SHOW TASK BY INDEX]");
        System.out.println("[ENTER INDEX]");
        final Integer index = TerminalUtil.nextNumber();
        final Task task = taskService.findByIndex(index - 1);
        showTask(task);
    }

    @Override
    public void clearTasks() {
        System.out.println("[CLEAR TASKS]");
        taskService.clear();
    }

    @Override
    public void removeTaskById() {
        System.out.println("[REMOVE TASK BY ID]");
        System.out.println("[ENTER ID]");
        final String id = TerminalUtil.nextLine();
        taskService.removeById(id);
    }

    @Override
    public void removeTaskByIndex() {
        System.out.println("[REMOVE TASK BY INDEX]");
        System.out.println("[ENTER INDEX]");
        final Integer index = TerminalUtil.nextNumber();
        taskService.removeByIndex(index - 1);
    }

}
