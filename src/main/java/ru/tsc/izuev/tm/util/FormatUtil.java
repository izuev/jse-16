package ru.tsc.izuev.tm.util;

import ru.tsc.izuev.tm.constant.FormatConst;

public final class FormatUtil {

    private FormatUtil() {
    }

    private static String render(final long bytes, final String name) {
        return bytes + FormatConst.SEPARATOR + name;
    }

    private static String render(final double bytes) {
        return FormatConst.FORMAT.format(bytes);
    }

    private static String render(final long bytes, final long size, final String name) {
        return render((double) bytes / size) + FormatConst.SEPARATOR + name;
    }

    public static String formatBytes(final long bytes) {
        if ((bytes >= 0) && (bytes < FormatConst.KILOBYTE))
            return render(bytes, FormatConst.NAME_BITES);
        if ((bytes >= FormatConst.KILOBYTE) && (bytes < FormatConst.MEGABYTE))
            return render(bytes, FormatConst.KILOBYTE, FormatConst.NAME_KILOBYTES);
        if ((bytes >= FormatConst.MEGABYTE) && (bytes < FormatConst.GIGABYTE))
            return render(bytes, FormatConst.MEGABYTE, FormatConst.NAME_MEGABYTES);
        if ((bytes >= FormatConst.GIGABYTE) && (bytes < FormatConst.TERABYTE))
            return render(bytes, FormatConst.GIGABYTE, FormatConst.NAME_GIGABYTES);
        if (bytes >= FormatConst.TERABYTE)
            return render(bytes, FormatConst.TERABYTE, FormatConst.NAME_TERABYTES);
        return render(bytes, FormatConst.NAME_BITES_LONG);
    }

}
